package threads;

import java.util.ArrayList;

public class ThreadDemo extends Thread {
    private Thread t;
    private String threadName;
    public boolean isInited;
    public boolean isCompleted;
    public boolean isFinalized;
    ArrayList<ThreadDemo> toWaitExecuteThreads;
    ArrayList<ThreadDemo> toWaitFinalizeThreads;

    public ThreadDemo(String name, ArrayList<ThreadDemo> toWaitExecuteThreads, ArrayList<ThreadDemo> toWaitFinalizeThreads) {
        threadName = name;
        isInited = false;
        isCompleted = false;
        isFinalized = false;
        this.toWaitExecuteThreads = toWaitExecuteThreads;
        this.toWaitFinalizeThreads = toWaitFinalizeThreads;
    }

    public void run() {
        synchronized(this) {
            try {
                task_init();
                task(toWaitExecuteThreads);
                task_finalize(toWaitFinalizeThreads);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void task(ArrayList<ThreadDemo> toWaitThreads) throws InterruptedException {
        if (toWaitThreads != null) {
            boolean isReady = true;
            while (!isReady) {
                isReady = true;
                for (ThreadDemo t : toWaitThreads) {
                    System.out.println("------------------------------------ " + t.threadName);
                    if (!t.isCompleted) isReady = false;
                }
                if (isReady) break;
                try {
                    t.wait();
                } catch (InterruptedException e) {
                }
            }
        }
        System.out.println("Task started " +  threadName );
        try {
            for(int i = 5; i > 0; i--) {
                System.out.println(threadName + ": Counter   ---   "  + i);
                Thread.sleep(50);
            }
            isCompleted = true;
            System.out.println("Task finished " +  threadName );
        } catch (Exception e) {
            System.out.println("Thread  interrupted.");
        }
        notify();
    }

    private void task_init() throws InterruptedException {
        System.out.println("Initializing " +  threadName );
        Thread.sleep(500);
        System.out.println("Initializied " +  threadName );
        isInited = true;
    }

    private void task_finalize(ArrayList<ThreadDemo> toWaitThreads) throws InterruptedException {
        if (toWaitThreads != null) {
            boolean isReady = true;
            while (!isReady) {
                isReady = true;
                for (ThreadDemo t : toWaitThreads) {
                    if (!t.isCompleted) isReady = false;
                }
                if (isReady) break;
                try {
                    t.wait();
                } catch (InterruptedException e) {
                }
            }
        }
        System.out.println("Finalizing " +  threadName );
        Thread.sleep(500);
        System.out.println("Finalized " +  threadName );
        isFinalized = true;
    }
}
