package filesystem;

import java.util.ArrayList;
import java.util.List;

public class RecycleBin extends Directory {
    private List<File> children;
    private static final int MAX_ELEMS = 1000;

    public RecycleBin(String name) {
        super(name, MAX_ELEMS);
        children = new ArrayList<>();
    }

    public void insert(File child) {
        try {
            mutex.lock();
            children.add(child);
            child.setParentDirectory(this);
        } finally {
            mutex.unlock();
        }
    }

    public void clean() {
        try {
            mutex.lock();
            children.clear();
        } finally {
            mutex.unlock();
        }
    }
}
