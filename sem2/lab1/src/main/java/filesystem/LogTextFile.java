package filesystem;

import java.util.LinkedList;
import java.util.Queue;

public class LogTextFile extends File {
    private Queue<String> content;

    public LogTextFile(String name) {
        super(name);
        this.content = new LinkedList<>();
    }

    public String read() {
        try {
            mutex.lock();
            return String.join("\n", content);
        } finally {
            mutex.unlock();
        }
    }

    public void appendLine(String line) {
        try {
            mutex.lock();
            content.add(line);
        } finally {
            mutex.unlock();
        }
    }
}
