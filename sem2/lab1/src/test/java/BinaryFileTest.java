import filesystem.BinaryFile;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.util.HashSet;
import java.util.Set;

class BinaryFileTest {

    @Test
    void readEmptyTest() {
        BinaryFile file = new BinaryFile("file.bin");
        assertEquals("", file.read());

        file = new BinaryFile("file.bin", "");
        assertEquals("", file.read());
    }

    @Test
    void readTest() {
        BinaryFile file = new BinaryFile("file.bin", "file");
        assertEquals("file", file.read());
    }
}